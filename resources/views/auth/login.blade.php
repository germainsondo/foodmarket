<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Askbootstrap">
    <meta name="author" content="Askbootstrap">
    <link rel="icon" type="image/png" href="img/fav.png">
    <title>Swiggiweb - Online Food Ordering Website Template</title>
    <!-- Slick Slider -->
    <link rel="stylesheet" type="text/css" href="vendor/slick/slick.min.css"/>
    <link rel="stylesheet" type="text/css" href="vendor/slick/slick-theme.min.css"/>
    <!-- Feather Icon-->
    <link href="vendor/icons/feather.css" rel="stylesheet" type="text/css">
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <!-- Sidebar CSS -->
    <link href="vendor/sidebar/demo.css" rel="stylesheet">
</head>

<body>
<div class="login-page vh-100">
    <video loop autoplay muted id="vid">
        <source src="img/bg.mp4" type="video/mp4">
        <source src="img/bg.mp4" type="video/ogg">
        Your browser does not support the video tag.
    </video>
    <div class="d-flex align-items-center justify-content-center vh-100">
        <div class="px-5 col-md-6 ml-auto">
            <div class="px-5 col-10 mx-auto">
                <h2 class="text-dark my-0">Bienvenue sur Food Market</h2>
                <p class="text-50">Connectez-vous pour continuer</p>
                <form class="mt-5 mb-4" action="{{route('login')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1" class="text-dark">Email</label>
                        <input type="email" placeholder="Enter Email"
                               class="form-control @error('email') is-invalid @enderror" name="email"
                               value="{{ old('email') }}" id="exampleInputEmail1">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="password" class="text-dark">Mot de passe</label>
                        <input id="password" type="password" placeholder="Entrer le mot de passe"
                               class="form-control  @error('password') is-invalid @enderror" name="password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                        <button type="submit" class="btn btn-primary btn-lg btn-block">
                            Connectez-vous
                        </button>
                        <div class="py-2">
                            <button class="btn btn-lg btn-facebook btn-block">
                                <i class="feather-facebook"></i> Connecter
                                avec Facebook
                            </button>
                        </div>
                </form>
                <a href="#" class="text-decoration-none">
                    <p class="text-center">Mot de passe oublié?</p>
                </a>
                <div class="d-flex align-items-center justify-content-center">
                    <a href="#">
                        <p class="text-center m-0">Vous n'avez pas de compte ? S'inscrire</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<nav id="main-nav">
    <ul class="second-nav">
        <li><a href="home.html"><i class="feather-home mr-2"></i> Homepage</a></li>
        <li><a href="my_order.html"><i class="feather-list mr-2"></i> My Orders</a></li>
        <li>
            <a href="#"><i class="feather-edit-2 mr-2"></i> Authentication</a>
            <ul>
                <li><a href="login.html">Login</a></li>
                <li><a href="signup.html">Register</a></li>
                <li><a href="forgot_password.html">Forgot Password</a></li>
                <li><a href="verification.html">Verification</a></li>
                <li><a href="location.html">Location</a></li>
            </ul>
        </li>
        <li><a href="#"><i class="feather-heart mr-2"></i> Favorites</a></li>
        <li><a href="#"><i class="feather-trending-up mr-2"></i> Trending</a></li>
        <li><a href="#"><i class="feather-award mr-2"></i> Most Popular</a></li>
        <li><a href="#"><i class="feather-paperclip mr-2"></i> Restaurant Detail</a></li>
        <li><a href="#"><i class="feather-list mr-2"></i> Checkout</a></li>
        <li><a href="#"><i class="feather-check-circle mr-2"></i> Successful</a></li>
        <li><a href="#"><i class="feather-map-pin mr-2"></i> Live Map</a></li>
        <li>
            <a href="#"><i class="feather-user mr-2"></i> Profile</a>
            <ul>
                <li><a href="#">Profile</a></li>
                <li><a href="#">Delivery support</a></li>
                <li><a href="#">Contact Us</a></li>
                <li><a href="#">Terms of use</a></li>
                <li><a href="#">Privacy & Policy</a></li>
            </ul>
        </li>
        <li>
            <a href="#"><i class="feather-alert-triangle mr-2"></i> Error</a>
            <ul>
                <li><a href="#">Not Found</a>
                <li><a href="#"> Maintence</a>
                <li><a href="#">Coming Soon</a>
            </ul>
        </li>
        <li>
            <a href="#"><i class="feather-link mr-2"></i> Navigation Link Example</a>
            <ul>
                <li>
                    <a href="#">Link Example 1</a>
                    <ul>
                        <li>
                            <a href="#">Link Example 1.1</a>
                            <ul>
                                <li><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Link Example 1.2</a>
                            <ul>
                                <li><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="#">Link Example 2</a></li>
                <li><a href="#">Link Example 3</a></li>
                <li><a href="#">Link Example 4</a></li>
                <li data-nav-custom-content>
                    <div class="custom-message">
                        You can add any custom content to your navigation items. This text is just an example.
                    </div>
                </li>
            </ul>
        </li>
    </ul>
    <ul class="bottom-nav">
        <li class="email">
            <a class="text-danger" href="#">
                <p class="h5 m-0"><i class="feather-home text-danger"></i></p>
                Home
            </a>
        </li>
        <li class="github">
            <a href="#">
                <p class="h5 m-0"><i class="feather-message-circle"></i></p>
                FAQ
            </a>
        </li>
        <li class="ko-fi">
            <a href="#">
                <p class="h5 m-0"><i class="feather-phone"></i></p>
                Help
            </a>
        </li>
    </ul>
</nav>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="vendor/jquery/jquery.min.js"></script>
<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- slick Slider JS-->
<script type="text/javascript" src="vendor/slick/slick.min.js"></script>
<!-- Sidebar JS-->
<script type="text/javascript" src="vendor/sidebar/hc-offcanvas-nav.js"></script>
<!-- Custom scripts for all pages-->
<script type="text/javascript" src="js/osahan.js"></script>
</body>

</html>
