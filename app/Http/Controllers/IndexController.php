<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(is_null(\auth()->user())){
            return view('index');
        }
        if (Auth::user()->hasRole('administrator') || Auth::user()->hasRole('fournisseur')){
            return view('admin.index');
        }
            return view('index');

    }

}
